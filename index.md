layout: default.liquid
title: PlainText
---

### News

{% for post in collections.posts.pages limit:5 %}
<div class="post">
<h4>&rarr;<a href="{{ post.permalink }}">{{ post.title }}</a></h4>
<p>{{ post.content | truncate: 50 }}</p>
</div>
{% endfor %}
