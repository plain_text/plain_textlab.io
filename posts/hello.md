title: "Hello, PlainText!"
layout: default.liquid
---
PlainText is an umbrella site for tools focused on productivity, essentialism, and privacy. Many of the tools I am planning to put under PlainText are ones I made for myself, and I hope that they can help other people as well.
